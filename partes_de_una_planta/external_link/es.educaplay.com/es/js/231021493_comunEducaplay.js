// JavaScript Document

function cambiarCodigo(){
	var d=new Date();
	var time=d.getTime();
	$('#imgCodigo').attr('src','/registrar.php?action=cambiarImagen&time='+time);
}

function cambiarCodigoT(){
	var d=new Date();
	var time=d.getTime();
	$('#imgCodigoT2').attr('src','/registrar.php?action=cambiarImagenT&time='+time);
    if($('#imgCodigoT')) $('#imgCodigoT').attr('src',$("#imgCodigoT2").attr('src'));
}

    $.fn.clearForm = function() {
        return this.each(function() {
          var type = this.type, tag = this.tagName.toLowerCase();
          if (tag == 'form')
            return $(':input',this).clearForm();
          if (type == 'text' || type == 'password' || tag == 'textarea')
            this.value = '';
          else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
          else if (tag == 'select')
            this.selectedIndex = -1;
        });
      };


function mvFormulario(nombre,alto,ancho)
{
	var aux="#btn"+nombre;

	var nombre2="#"+nombre;
	$(aux).click(function(event){
		event.preventDefault();
		$(this).addClass( "activo" );

//		$(nombre2).css({visibility: "visible",display: "none", height: alto, width: ancho}).slideDown(350);		
		$(nombre2).css({visibility: "visible",display: "none"}).slideDown(350);		
    })
	aux=aux+"Close";
	$(aux).click(function(event){
		event.preventDefault();
		
		$(nombre2).slideUp(350, function(){
			$(nombre2).removeClass( "activo" );		
			$(nombre2).css({display: "none"});
		});
		
    })

}

function mvFormularioLimpia(nombre,alto,ancho)
{
	var aux="#btn"+nombre;

	var nombre2="#"+nombre;
	$(aux).click(function(event){
		event.preventDefault();
		$(this).addClass( "activo" );

//		$(nombre2).css({visibility: "visible",display: "none", height: alto, width: ancho}).slideDown(350);		
		$(nombre2).css({visibility: "visible",display: "none"}).slideDown(350);		
    })
	aux=aux+"Close";
	$(aux).click(function(event){
		event.preventDefault();
		$("#form"+nombre).clearForm();
		$(nombre2).slideUp(350, function(){
			$(nombre2).removeClass( "activo" );		
			$(nombre2).css({display: "none"});
		});
		
    })

}
function mvFormulario2(nombre)
{
	var aux="#btn"+nombre;

	var nombre2="#"+nombre;
	$(aux).click(function(event){
		event.preventDefault();
		$(this).addClass( "activo" );

		$(nombre2).css({visibility: "visible",display: "none"}).slideDown(350);		
    })
	aux=aux+"Close";
	$(aux).click(function(event){
		event.preventDefault();
		$("#form"+nombre).clearForm();
		$(nombre2).slideUp(350, function(){
			$(nombre2).removeClass( "activo" );		
			$(nombre2).css({display: "none"});
		});
		
    })
}

function reproducir(idActividad,audio){
	var url='verActividad.php?action=reproducirAudio&idActividad='+idActividad+'&audio='+audio;
	window.open(url, 'Audio','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=300,height=45');
}
function reproducir2(idActividad,audio){
	try{

		var url='verActividad.php?action=reproducirAudio&idActividad='+idActividad+'&audio='+audio;
		$.showAkModal(url,'',300,75);
	}catch(ee){
		reproducir(idActividad,audio);	
	}
}
function reproducirVid(idActividad,video){
	try{
		var url=video;
        $.showAkModal(url,'',450,300);
	}catch(ee){
		reproducir(idActividad,audio);	
	}
}

$(document).ready(function(){
	
	$(".headerMain__panel__login__tickets input").keyup(function(event) {
	  id = parseInt(this.id.substr(this.id.length - 1));
	  if (id < 3) {
			if (this.value.length >= 3) {			
				$("#headerMain__panel__login__campo__" + (id + 1)).focus();
			}
		}
		if (id > 1) {
			if (this.value.length == 0) {
				if (event.which == 8) {
					$("#headerMain__panel__login__campo__" + (id - 1)).focus();
				}
			}
		}		
	});
	
	$("a.linkFollow").bind("click",function(e){
		e.preventDefault();
		$usuario=$(this);
		educaplay_suscribeUser($usuario);
	});
	
	$("a.linkUnfollow").bind("click",function(e){
		e.preventDefault();
		$usuario=$(this);
		educaplay_unsuscribeUser($usuario);
	});
	
	
	$("#notificacionesClose").bind("click",function(e){
		e.preventDefault();
		$("#linkNotificaciones").trigger("click");
	});
	$("#mensajesClose").bind("click",function(e){
		e.preventDefault();
		$("#linkMensajesPrivados").trigger("click");
	});

	$("a#linkMensajesPrivados").click(function(e){
		e.preventDefault();
		e.stopPropagation();

		if($("#linkNotificaciones").hasClass('openPreview')){
			$("#linkNotificaciones").trigger('click');
		}
		
		if($(this).hasClass('openPreview')){
			$("#linkMensajesPrivados span").html("0");
			$("#linkMensajesPrivados").attr("title","");
			$(this).removeClass("openPreview").removeClass("activo");
			$("#userMensajes").slideUp(350);
		}
		else{
			$(this).addClass('openPreview');
			$("#userMensajes .notificacionesDisplay ul").empty().append("<li class='loadAlerts'>"+$(document).data("loading")+"...</li>");
			$.getJSON("/mensajes.php",
				function(data){
					if(data.mensajes.length){
						for(i=0;i<data.mensajes.length;i++){
							$("#userMensajes .notificacionesDisplay ul").append("<li>"+data.mensajes[i].DISPLAY_TEXT+"</li>");
						}
					}
					else{
						$("#userMensajes .notificacionesDisplay ul").append('<li class="noAlerts">'+$(document).data("noMessages")+'</li>');
					}
					
					$("#userMensajes .notificacionesDisplay ul li.loadAlerts").remove();
					$("#userMensajes").slideDown(350);
				}
			);
		}
	});

	$("a#linkNotificaciones").click(function(e){
		e.preventDefault();
		e.stopPropagation();
		
		if($("#linkMensajesPrivados").hasClass('openPreview')){
			$("#linkMensajesPrivados").trigger('click');
		}
		
		if($(this).hasClass('openPreview')){
			$("#linkNotificaciones span").html("0");
			$(this).removeClass("openPreview").removeClass("activo");
			$("#userNotificaciones").slideUp(350);
		}
		else{
			$(this).addClass('openPreview');

			$("#userNotificaciones .notificacionesDisplay ul").empty().append("<li class='loadAlerts'></li>");
			$.getJSON("/notificaciones.php",
				function(data){
					if(data.notificaciones.length){
						var xtraClass="";
						for(i=0;i<data.notificaciones.length;i++){
							xtraClass="";
							if(data.notificaciones[i].ES_NUEVA_NOTIFICACION=='1'){
								xtraClass=" nuevaNotificacion";
							}
							
							$("#userNotificaciones .notificacionesDisplay ul").append("<li class='"+data.notificaciones[i].TIPO_NOTIFICACION + xtraClass+"'>"+data.notificaciones[i].DISPLAY_TEXT+"</li>");
						}
					}
					else{
						$("#userNotificaciones .notificacionesDisplay ul").append('<li class="noAlerts">'+$(document).data("noNotifications")+'</li>');
					}
					$("#userNotificaciones .notificacionesDisplay ul li.loadAlerts").remove();
					$("#userNotificaciones").slideDown(350);
				}
			);
		}
		
		return false;
    });
	
	$("a.noclick, a.noclick *").css("cursor","default");
	$("a.noclick").click(function(e){
		e.preventDefault();
	});
});
