var contMapa = 0;
var paginador = 0;
var idTimeBusca = 0;
var maxMostrar = 10;
var act_x_pag = 14;
var tieneSistema = 0;
var cantidadesAsig = new Array();
var busqueda_bloqueada = false;

$(document).ready(function(e) {
	
		$('.dropit-menu').dropit();
		$('.box-search input[type="text"]').focus(function(e) {
			$(this).parent().addClass('box-search-active');
		}).blur(function() {
     		$(this).parent().removeClass('box-search-active');
		});
	
    regeneraMapa(regionS);
    comprobarTieneSistema(regionS,1);
  	
    $('.btnIdioma').click(function(e){
    	e.preventDefault();
    	$('#titIdioma').attr('data-idioma',$(this).attr('title'));
    	$('#titIdioma').html($(this).html());
    	buscaActividades(0,0,cantActivo);
    });
    
    $('.btnTipo').click(function(e){
    	e.preventDefault();
    	$('#titTipo').attr('data-tipo',$(this).attr('data-tipo'));
    	$('#titTipo').html($(this).html());
    	buscaActividades(0,0,cantActivo);
    });
    
    $('.btnOrden').click(function(e){
    	e.preventDefault();
    	$('#titOrden').attr('data-orden',$(this).attr('data-orden'));
    	$('#titOrden').html($(this).html());
    	buscaActividades(0,0,0);
    });

		cargarSlider();
		
    $('.select-box').scombobox();
		
    $("#selPais .scombobox-list p").bind('click',function(){
        setTimeout(function(){
            var nPais = $("#selPais .scombobox-list p[class='scombobox-hovered'] span").html();
            var cPais = "ES";
            $("#selPais option").each(function(){
                if($(this).html()==nPais) cPais = $(this).val();
            });
            $("#selPais option[selected='selected']").removeAttr('selected');
            $("#selPais option[value='"+cPais+"']").attr('selected','selected');
            $('#selPais').scombobox();
            regeneraMapa(cPais);
            comprobarTieneSistema(cPais,0);
        },10);
    });
    
    $(".jslider-value").css('display','none');
    $(".jslider-pointer").bind("mousedown",function(){ $(".jslider-pointer").css('z-index','2'); });
    
    if(!cantActivo)
    {
        var countItems = $('#resultados .navList li').length;
	    var countItemsVisibles = 5;
    
        $('.moreResults').hide();
    	if (countItems > countItemsVisibles){
    		$('.moreResults').show();
    		for ( countItems; countItems > countItemsVisibles; countItems-- ) {
    			 $('#resultados .navList li:nth-child('+countItems+')').hide();
    		}
    	}
        
        $('.moreResults a').click(function(e){
            e.preventDefault();
    		$('#resultados .navList li').show();
    		countItems = $('#resultados .navList li').length;
    		countItemsVisibles = countItemsVisibles + 5;
    		$('.moreResults').hide();
    		if (countItems > countItemsVisibles){
    			$('.moreResults').show();
    			for ( countItems; countItems > countItemsVisibles; countItems-- ) {
    				 $('#resultados .navList li:nth-child('+countItems+')').hide();
    			}
    		}
   		});
    }
    
});

function cargarSlider() {
  $("#mEdad").slider({ 
    from: 2,
    to: 21,
    scale: ["-",'|',4,'|','|',7,'|','|',10,'|','|',13,'|','|',16,'|','|',19,'|',"+20"],
    limits: false,
    step: 1,
    callback: function(){ 
    	estableceEdad();
    }
	});
}

function recargarSlider() {
	$("#mEdad").val('2;21');
	$("#mEdad").data('jslider',null);
	$(".jslider").remove();
	cargarSlider();
	$(".jslider-value").hide();
}

$(document).keypress(function(e) {
  if(e.which == 13) {
		$("#buscar").click();
  }
});

	$('.iCloseFilter').click(function(){
		$('#box').val('');	
		$('.navList > li').show();
		$('.no-results-found').remove();
		$('.iCloseFilter').hide();
	});

	$('#box').keyup(function(){
		var valThis = $(this).val().toLowerCase();
		var noresult = 0;
		$('.iCloseFilter').show();
		if(valThis == ""){
			$('.navList > li').show();
			noresult = 1;
			$('.no-results-found').remove();
			$('.iCloseFilter').hide();
		} else {
			$('.navList > li').each(function(){
				var text = $(this).text().toLowerCase();
				var match = text.indexOf(valThis);
				if (match >= 0) {
					$(this).show();
					noresult = 1;
					$('.no-results-found').remove();
				} else {
					$(this).hide();
				}
			});
        };
		if (noresult == 0) {
			$(".navList").append("<li class='no-results-found'>"+getMensaje(1)+"</li>");
		}
	});
    
	
	
	$('.volver-cursos a').click(function(e){
		e.preventDefault();
		$('.container-slider').toggleClass('slider2');
	});
    
    $(".bodyAccordion").hide();
	$(".headAccordion").click(function(e) {
    	$(this).toggleClass("iCollapse-close");
		$(this).next(".bodyAccordion").slideToggle("300").addClass('accordionOpen');
	});
	
   $(".headFilter-iCollapse").click(function(e) {
    	$(this).toggleClass("iCollapse-close");
		$(this).parent().next(".bodyFilter").slideToggle("300");
	});
    
	$('.containerCursos .iListArrowLeft').click(function(e){
	    e.preventDefault();
        if(!cantActivo)
        {
            peticionAsignaturas($(this).attr("data-id"),$(this).text());
            var txtTag = $(this).text();
        }
        else
        {
            peticionAsignaturas($(this).attr("data-id"),$(this).find('.texto').text());
            var txtTag = $(this).find('.texto').text();
        }
				crearEtiqueta(txtTag,'curso',$(this).attr("data-id"));
				buscaActividades(0,0,0);
        $("#bloqueEdad").hide();
        $("#bloqueEdadNo").show();
        if(!$("#bloqueEdadNo .iCollapse-close").length) $("#bloqueEdadNo .iconDir").click();
        $("#edadCurso").html($(this).attr("data-edad"));
        $("#edadCurso2").html($(this).attr("data-edad"));
	});
    
    $('.containerAreaConocimiento .iListArrowLeft').click(function(e){
        e.preventDefault();
		if(!cantActivo) var txtTag = $(this).text();
        else var txtTag = $(this).find('.texto').text();
			crearEtiqueta(txtTag,'area',$(this).attr("data-id"));
			buscaActividades(0,0,0);
	});
	
		$(document).on("click", "#btnLimpiar", function() {
			$("#busqueda").val('');
			$('#titTipo').attr('data-tipo','');
    	$('#titTipo').html($(".btnTipo[data-tipo='']").html());
    	$('#titOrden').attr('data-orden','novedades');
    	$('#titOrden').html($(".btnOrden[data-orden='novedades']").html());
    	
    	$('#titIdioma').attr('data-idioma',idiomaG.toUpperCase());
    	$('#titIdioma').html($(".btnIdioma[title='" + idiomaG.toUpperCase() + "']").html());
    	
			if($(".container-tagFilter span[data-tipo='edad']").length) {
				recargarSlider();
			}
			if($(".container-tagFilter span[data-tipo='curso']").length) {
		    $("#bloqueEdad").show();
		    $("#bloqueEdadNo").hide();
		    if($(".slider2").length) $(".volver-cursos a").click();
		    $(".iCollapse-close").click();
			}
			$(".container-tagFilter span").remove();
			buscaActividades(0,0,cantActivo);
		});
    
	$(document).on("click", ".tagFilter-close", function() {
	   $(this).parent().remove();
       var pet = cantActivo;
				switch ($(this).parent().attr('data-tipo')) {
					case 'asignatura':
					case 'curso':
					case 'area':
						pet = 0;
					break;
				}
       if($(this).parent().attr('data-tipo')=='busqueda') {
					$("#busqueda").val('');
     	 } else {
     	 		if($(this).parent().attr('data-tipo')=='edad') {
     	 			recargarSlider();
     	 		} else {
			       if($(this).parent().attr('data-tipo')=='asignatura')
			       {
			            $("#listaAsignaturas input[data-id-asignatura='"+$(this).parent().attr('data-id')+"']").parent().click();
			       }
			       if(!$(".container-tagFilter span[data-tipo='curso']").length)
			       {
			            $(".container-tagFilter span[data-tipo='asignatura']").remove();    
			            $("#bloqueEdad").show();
			            crearEtiqueta($("#mEdad").val(),"edad",$("#mEdad").val());
			            $("#bloqueEdadNo").hide();
			            if($(".slider2").length) $(".volver-cursos a").click();
			            $(".iCollapse-close").click();
			       }
			    }
	     }
       buscaActividades(0,0,pet);
	});
    
    $(".numPags").live("click",function(e){
    		e.preventDefault();
        $('html, body').animate({scrollTop:0}, 300);
        $(".numPags").removeClass("activo");
        $(this).addClass("activo");
        buscaActividades($(this).attr("data-pag"),1,0);
    });
    
    $(".numPags1").live("click",function(e){
    		e.preventDefault();
        var act = parseInt($(".activo").attr("data-pag"));
        if(!$(".numPagsEtc1").hasClass('oculto')){
            if($(".numPags[data-pag='"+parseInt(act-1)+"']").hasClass('oculto')){
                $(".numPags[data-pag='"+parseInt(act-1)+"']").removeClass('oculto');
                if(act == 1) $(".numPagsEtc1").addClass('oculto');
                $(".numPags[data-pag='"+parseInt(act+maxMostrar-1)+"']").addClass('oculto');
                $(".numPagsEtc2").removeClass('oculto');
            }   
        }
        if(act != 0) $(".numPags[data-pag='"+parseInt(act-1)+"']").click();
    });
    
    $(".numPags2").live("click",function(e){
    		e.preventDefault();
        var act = parseInt($(".activo").attr("data-pag"));
        var max = parseInt(numActividadesTotal/act_x_pag);
        if(!$(".numPagsEtc2").hasClass('oculto')){
            if(!$(".numPags[data-pag='"+parseInt(act+1)+"']").length){
                $("<div class='pag numPags' id='b_"+parseInt(act+2)+"' data-pag='"+parseInt(act+1)+"'>"+parseInt(act+2)+"</div>").insertBefore($(".numPagsEtc2"));
                if(act == max-1) $(".numPagsEtc2").addClass('oculto');
                $(".numPags[data-pag='"+parseInt(act-maxMostrar+1)+"']").addClass('oculto');
                $(".numPagsEtc1").removeClass('oculto');
            }
            else if($(".numPags[data-pag='"+parseInt(act+1)+"']").hasClass('oculto')){
                $(".numPags[data-pag='"+parseInt(act+1)+"']").removeClass('oculto')
                if(act == max-1) $(".numPagsEtc2").addClass('oculto');
                $(".numPags[data-pag='"+parseInt(act-maxMostrar+1)+"']").addClass('oculto');
                $(".numPagsEtc1").removeClass('oculto');
            }  
        }
        if(act != max) {
        	$(".numPags[data-pag='"+parseInt(act+1)+"']").click();
        }
    });
    
    $("#buscar").click(function(){
    	if (!busqueda_bloqueada) {
    		busqueda_bloqueada = true;
	      $(".container-tagFilter span[data-tipo='busqueda']").remove();
	  		crearEtiqueta($("#busqueda").val(),'busqueda',$("#busqueda").val());
	      buscaActividades(0,0,cantActivo);
	      setTimeout(function(){ busqueda_bloqueada = false; }, 1000);
	    }
    });
    
function mostrarFiltro() {
	if (($(".container-tagFilter span").length) || ($('#titTipo').attr('data-tipo') != '') || ($('#titOrden').attr('data-orden') != 'novedades') || ($('#titIdioma').attr('data-idioma') != idiomaG.toUpperCase())) {
		$(".filtroResultados").html('<span id="btnLimpiar">'+txtFiltro+'</span>');
	}
}

function borrarFiltro() {
	$(".filtroResultados").html('');
}

function regeneraMapa(reg)
{
    contMapa++;
    $("#dMap").html("<div id='vmap' class='draw-map'></div>");
	$('#vmap').vectorMap(
		{
			map: 'world_en',
			backgroundColor: '#a5bfdd',
			borderColor: '#818181',
			borderOpacity: 0.25,
			borderWidth: 1,
			color: '#f4f3f0',
			hoverOpacity: 0.75,
			selectedRegion: reg,
			selectedColor: '#c9dfaf',
			enableZoom: true,
			showTooltip: true,
			scaleColors: ['#b6d6ff', '#005ace'],
            onRegionClick: function(element, code, region)
            {
                $("#selPais option[selected='selected']").removeAttr('selected');
                $("#selPais option[value='"+code.toUpperCase()+"']").attr('selected','selected');
                $('#selPais').scombobox();
                comprobarTieneSistema(code.toUpperCase(),0);
            }
		}            
    );
    recolocaMapa(reg);
}

function recolocaMapa(cPais)
{
    if($("#jqvmap"+contMapa+"_"+cPais.toLowerCase()).length)
    {
        var dx = $("#jqvmap"+contMapa+"_"+cPais.toLowerCase())[0].getBBox().x;
        var dy = $("#jqvmap"+contMapa+"_"+cPais.toLowerCase())[0].getBBox().y;  
    }
    else
    {
        var dx = 400;
        var dy = 200; 
    }
    dx = parseInt(dx)-150;
    dy = parseInt(dy)-100;
    
    if(cPais == 'US'){
        dx = 30;
        dy = 50;
    }
    
    $("svg g").attr("transform","translate("+-dx+","+-dy+")");
}

function estableceEdad()
{
    var valEdad = $("#mEdad").val();
    var aValEdad = valEdad.split(';');
    if(aValEdad[0]==2)aValEdad[0]="-";
    $("#edadDe").html(aValEdad[0]);
    if(aValEdad[1]==21)aValEdad[1]="+20";
    $("#edadA").html(aValEdad[1]);
    $(".container-tagFilter span[data-tipo='edad']").remove();
		crearEtiqueta(valEdad,"edad",valEdad);
    buscaActividades(0,0,cantActivo);
}

function peticionAsignaturas(id,nombre)
{
    var postData = { curso: id };
    $.ajax(
    {
	     url : "/asignaturas.php",
	     type: "POST",
	     data : postData,
	     dataType: 'json',
	     cache: false, 
	     success:function(jsonObj, textStatus, jqXHR) 
	     {
            if(jsonObj.asignaturas.length)
		    {	
		        $("#listaAsignaturas").html("");
                var cadenaAsignaturas = "";
                for(i=0;i<jsonObj.asignaturas.length;i++)
                {
                   if(!cantActivo)cadenaAsignaturas += "<li><a href='#' data-url='"+jsonObj.asignaturas[i].URL+"' data-url-red='"+jsonObj.asignaturas[i].URL_RED+"' class='asignaturasEnlace'><label><input type='checkbox' name='checkbox' data-id-asignatura='"+jsonObj.asignaturas[i].ID+"' data-text-asignatura='"+jsonObj.asignaturas[i].NOMBRE+"'>"+jsonObj.asignaturas[i].NOMBRE+"</label></a></li>";
                   else cadenaAsignaturas += "<li><a href='#' data-url='"+jsonObj.asignaturas[i].URL+"' data-url-red='"+jsonObj.asignaturas[i].URL_RED+"' class='asignaturasEnlace'><label><input type='checkbox' name='checkbox' data-id-asignatura='"+jsonObj.asignaturas[i].ID+"' data-text-asignatura='"+jsonObj.asignaturas[i].NOMBRE+"'><span class='texto'>"+jsonObj.asignaturas[i].NOMBRE+"</span><span class='indicadorCantidad'> (<span class='cantAsignaturas' data-id='"+jsonObj.asignaturas[i].ID+"'></span>)</span></label></a></li>"; 
                }
                $("#listaAsignaturas").html(cadenaAsignaturas);
                if(cantidadesAsig.length) cargaCantidadesAsig();
                $(".titCursoAsignaturas").html(nombre);
                $(".listItemsFiltro input[type='checkbox']").change(function(e) {
                    e.stopPropagation();
                    if(!cantActivo) var txtTag = $(this).parent().text();
                    else var txtTag = $(this).parent().find('.texto').text();
                    if (this.checked) {
      			       crearEtiqueta(txtTag,"asignatura",$(this).attr('data-id-asignatura'));
      			       buscaActividades(0,0,0);
                    } else {
        				$('.tagFilter-txt:contains("'+ txtTag +'")').parent().remove();
                        buscaActividades(0,0,0);
        			}
        		});
                $('.asignaturasEnlace').click(function(e){
                    if(e.target == $(this).children('input:checkbox'))
                    {
                        e.preventDefault();   
                    }
                });
            }
            else
            {
                var cadenaAsignaturas = getMensaje(2);
                $("#listaAsignaturas").html(cadenaAsignaturas); 
            }
            return;
	     },
		 error: function(jqXHR, textStatus, errorThrown) 
		 {
		    var cadenaAsignaturas = getMensaje(3);
            $("#listaAsignaturas").html(cadenaAsignaturas);    	
		 }
    }); 
}

function crearEtiqueta(txtTag,tipo,id) {
	
    var existe = $(".container-tagFilter span[data-tipo='"+tipo+"'][data-id='"+id+"']").length;
    var cancelar = false;
    
    if(!existe)
    {
        if(tipo=='curso')
        {
        		$(".container-tagFilter span[data-tipo='edad']").remove();
            $(".container-tagFilter span[data-tipo='area']").remove();
            $(".container-tagFilter span[data-tipo='curso']").remove();
            $(".container-tagFilter span[data-tipo='asignatura']").remove(); 
        }
        else if(tipo=='area')
        {
            $(".container-tagFilter span[data-tipo='curso']").remove();
            $(".container-tagFilter span[data-tipo='area']").remove();
            $(".container-tagFilter span[data-tipo='asignatura']").each(function(){
                $(this).remove();
                $("#listaAsignaturas input[data-id-asignatura='"+$(this).attr('data-id')+"']").parent().click();
            });
						crearEtiqueta($("#mEdad").val(),"edad",$("#mEdad").val());
            $("#bloqueEdad").show();
            $("#bloqueEdadNo").hide();
            if($(".slider2").length) $(".volver-cursos a").click();
        } else if(tipo=='edad') {
        	if (id == '2;21') {
        		cancelar = true;
        	} else {
        		txtTag = id.replace(';','-') + ' ' + txtAnios;
        	}
        }
        if (!cancelar) {
	        var htmlContent =  "<span class='tagFilter' data-tipo='"+tipo+"' data-id='"+id+"' title='"+txtTag+"'><span class='tagFilter-txt'>" + txtTag +"</span><span class='tagFilter-close'><span class='iconDir'></span></span></span>";
	        $(htmlContent).insertBefore( ".container-tagFilter .cl" );	   
	      }
    }
}

function crearEtiquetaU(txtTag,tipo,id) {
    var htmlContent =  "<span class='tagFilter' data-tipo='"+tipo+"' data-id='"+id+"' title='"+txtTag+"'><span class='tagFilter-txt'>" + txtTag +"</span><span class='tagFilter-close'><span class='iconDir'></span></span></span>";
    $(htmlContent).insertBefore( ".container-tagFilter .cl" );	   
}

function comprobarTieneSistema(pais,etags) {
    $('.container-slider').removeClass('slider2');
    var postData = { codPais: pais };
    $.ajax(
    {
	     url : "/compruebaSistema.php",
	     type: "POST",
	     data : postData,
	     dataType: 'json',
	     cache: false, 
	     success:function(jsonObj, textStatus, jqXHR) 
	     {
            if(jsonObj.tieneSistema == 1)
		    {	
		        $("#bloqueCursos").show();
                $("#bloqueCursosNo").hide();
                regeneraCursos(pais,etags);
            }
            else
            {
                $("#bloqueCursos").hide();
                $("#bloqueCursosNo").show();
                $("#bloqueEdad").show();
                $("#bloqueEdadNo").hide();
                $("#nombrePaisSt").html($("#selPais .scombobox-list p[class='scombobox-hovered'] span").html());
            }
            return;
	     },
		 error: function(jqXHR, textStatus, errorThrown) 
		 {
		      $("#bloqueCursos").hide();
              $("#bloqueCursosNo").hide();
              $("#bloqueEdad").show();
              $("#bloqueEdadNo").hide();
		 }
    }); 
}

function regeneraCursos(pais,etags)
{	
    var postData = { codPais: pais };
    $.ajax(
    {
	     url : "/listadoCursos.php",
	     type: "POST",
	     data : postData,
	     dataType: 'json',
	     cache: false,
	     success:function(jsonObj, textStatus, jqXHR) 
	     {
            if(jsonObj.cursos.length)
		    {	
		        var cadenaCursos = generaCursos(jsonObj.cursos);
                $("#accordion").html(cadenaCursos);
                $('.bodyAccordion a').click(function(e){
        			e.preventDefault();
        			$('.container-slider').toggleClass('slider2');
        		});
                $(".bodyAccordion").hide();
                $(".headAccordion").click(function(e) {
                	$(this).toggleClass("iCollapse-close");
        			$(this).next(".bodyAccordion").slideToggle("300").addClass('accordionOpen');
            	});

                $('.containerCursos .iListArrowLeft').click(function(e){
        		    e.preventDefault();
                    if(!cantActivo)
                    {
                        peticionAsignaturas($(this).attr("data-id"),$(this).text());
     			        var txtTag = $(this).text();
                    }
                    else
                    {
                        peticionAsignaturas($(this).attr("data-id"),$(this).find('.texto').text());
     			        var txtTag = $(this).find('.texto').text();
        			}
        			crearEtiqueta(txtTag,'curso',$(this).attr("data-id"));
        			buscaActividades(0,0,0);
                    $("#bloqueEdad").hide();
                    $("#bloqueEdadNo").show();
                    if(!$("#bloqueEdadNo .iCollapse-close").length) $("#bloqueEdadNo .iconDir").click();
                    $("#edadCurso").html($(this).attr("data-edad"));
                    $("#edadCurso2").html($(this).attr("data-edad"));
	        		});
	        		
	        		if (cantActivo) {
					  		cargaCantidades(1);
					  	}
        		
            }
            else
            {
                $("#bloqueCursos").hide();
                $("#bloqueCursosNo").hide();
                $("#bloqueEdad").show();
                $("#bloqueEdadNo").hide();
            }
            return;
	     },
		 error: function(jqXHR, textStatus, errorThrown) 
		 {
		      $("#bloqueCursos").hide();
              $("#bloqueCursosNo").hide();
              $("#bloqueEdad").show();
              $("#bloqueEdadNo").hide();
		 }
    }); 
}

function generaCursos(cursos)
{
    var cadena = "";
    var anterior = "";
    var abierto = 0;
    for(i=0;i<cursos.length;i++)
    {
        if(anterior != cursos[i]["NIVEL"])
        {
            anterior=cursos[i]["NIVEL"];
            if(abierto == 1)
            {
                cadena += "</ul></div>";
                abierto = 0;
            }
            abierto=1;
            cadena += "<div class='headAccordion'>";
            cadena += "<span class='iconDir'></span>";
            if(!cantActivo) cadena += "<h3>"+cursos[i]['NIVEL']+"</h3>";
            else cadena += "<h3>"+cursos[i]['NIVEL']+"<span class='indicadorCantidad'> <span class='cantNivel'></span></span></h3>";
            cadena += "</div>";
            cadena += "<div class='bodyAccordion'>";
            cadena += "<ul>";
        }
        if(!cantActivo) {
        	cadena += "<li><a href='#' class='iListArrowLeft' id='c_"+cursos[i]['ID']+"' data-id='"+cursos[i]['ID']+"' data-edad='"+cursos[i]['EDAD']+"'>"+cursos[i]['NOMBRE']+" - "+cursos[i]['NIVEL']+"</a></li>";
        } else {
        	cadena += "<li><a href='#' class='iListArrowLeft' id='c_"+cursos[i]['ID']+"' data-id='"+cursos[i]['ID']+"' data-edad='"+cursos[i]['EDAD']+"'><span class='texto'>"+cursos[i]['NOMBRE']+" - "+cursos[i]['NIVEL']+"</span><span class='indicadorCantidad'> (<span class='cantCursos' data-id='"+cursos[i]['ID']+"'></span>)</span></a></li>";
        }
    }
    cadena += "</ul></div>";
    return cadena;    
}

function cargaCantidades(etags) {

  var pais = $("#selPais option[selected='selected']").val();
  var edad = "";
  if($("#bloqueEdad").css("display")=="block") var edad = $("#mEdad").val();
  var idioma = $('#titIdioma').attr('data-idioma');
  var tipos = $('#titTipo').attr('data-tipo');
  var busqueda = "";
  if($("#busqueda").length) busqueda = $("#busqueda").val();

	var postData = { valPais: pais, valEdad: edad, valIdioma: idioma, valTipos: tipos, buscar: busqueda, valEtags: etags };
	
  $.ajax(
  {
     url : "/cantidadesTesauro.php",
     type: "POST",
     data : postData,
     dataType: 'json',
     cache: false,
     success:function(jsonObj, textStatus, jqXHR) 
     {
     			if(jsonObj.totalesEtag) cargaCantidadesEtag(jsonObj.totalesEtag);
          if(jsonObj.totalesCurso) cargaCantidadesCurso(jsonObj.totalesCurso);
          if(jsonObj.totalesAsig) cantidadesAsig = jsonObj.totalesAsig;
          if(cantidadesAsig.length && $(".slider2").length) cargaCantidadesAsig();
          return;
     }
  });
}

function buscaActividades(pag,paginador,petCants)
{
		borrarFiltro();
		$(".numResultados").html('');
    $("#listActividadesTes").html("<div class='msgTesauro msgLoading'>Loading...</div>");
    if(!paginador){
        paginador = 0;
        $("#paginador").html("");
    } 
    
    clearTimeout(idTimeBusca);
    idTimeBusca = setTimeout(function(){
        
        var ord = $('#titOrden').attr('data-orden');
        var pais = $("#selPais option[selected='selected']").val();
        var edad = "";
        if($("#bloqueEdad").css("display")=="block") var edad = $("#mEdad").val();
        var idioma = $('#titIdioma').attr('data-idioma');
        var tipos = $('#titTipo').attr('data-tipo');
        var areas = "";
        var cursos = "";
        var asignaturas = "";
				
        $(".container-tagFilter span").each(function(){
             if($(this).attr('data-tipo') == "area") areas += $(this).attr("data-id")+";";
             if($(this).attr('data-tipo') == "curso") cursos += $(this).attr("data-id")+";";
             if($(this).attr('data-tipo') == "asignatura") asignaturas += $(this).attr("data-id")+";";
        });
				
        var busqueda = "";
        if($("#busqueda").length) busqueda = $("#busqueda").val();
        var postData = { valPais: pais, valEdad: edad, valIdioma: idioma, valTipos: tipos, valAreas: areas, valCursos: cursos, valAsignaturas: asignaturas, pagina: pag, orden: ord, pedirCants: petCants, buscar: busqueda, valact_x_pag: act_x_pag };
        $.ajax(
        {
    	     url : "/buscadorActividadesTesauro.php",
    	     type: "POST",
    	     data : postData,
    	     dataType: 'json',
    	     cache: false,
    	     success:function(jsonObj, textStatus, jqXHR) 
    	     {
                if(jsonObj.actividades.length)
    		    {	
    		        $(".numResultados").html('<span id="numActividades"><strong>'+jsonObj.total+'</strong></span> '+txtResultados);
    		        
                    if((jsonObj.total > act_x_pag)&&(paginador == 0))
                    {
                        paginador = 1;
                        generaPaginador(jsonObj.total); 
                    }
                    else
                    {
                        if(jsonObj.total <= act_x_pag){
                            paginador = 0;
                            $("#paginador").html("");
                        }
                    }
                    generaActVis(jsonObj.actividades);
                }
                else
                {
                    $(".numResultados").html('<span id="numActividades"><strong>0</strong></span> '+txtResultados);
                    $("#listActividadesTes").html("<div class='msgTesauro msgSinActividades'>"+getMensaje(4)+"</div>");
                }
                if(jsonObj.totalesCurso && petCants) cargaCantidadesCurso(jsonObj.totalesCurso);
                if(jsonObj.totalesEtag && petCants) cargaCantidadesEtag(jsonObj.totalesEtag);
                if(jsonObj.totalesAsig) cantidadesAsig = jsonObj.totalesAsig;                
                if(cantidadesAsig.length && $(".slider2").length) cargaCantidadesAsig();                
                mostrarFiltro();
                return;
    	     },
    		 error: function(jqXHR, textStatus, errorThrown) 
    		 {
    		       $(".numResultados").html('<span id="numActividades"><strong>0</strong></span> '+txtResultados);
    		       $("#listActividadesTes").html("<div class='msgTesauro msgError'>"+getMensaje(5)+"</div>"); 
    		 }
        });
    },10);
}

function generaPaginador(total)
{		
    numActividadesTotal = total;
    var pags = parseInt(total/act_x_pag);
    var ocultos = 0;
    if(total%act_x_pag != 0) pags++;
    $("#paginador").append("<div class='pag numPags1' id='b_ini'>&#8592;</div>");
    $("#paginador").append("<div class='pag numPagsEtc1 oculto'>...</div>");
    if(maxMostrar > pags) var marcador = pags;
    else var marcador = maxMostrar;
    for(i=1;i<=marcador;i++)
    {
        if(i==1) $("#paginador").append("<div class='pag numPags activo' id='b_"+i+"' data-pag='"+parseInt(i-1)+"'>"+i+"</div>"); 
        else $("#paginador").append("<div class='pag numPags' id='b_"+i+"' data-pag='"+parseInt(i-1)+"'>"+i+"</div>");
    }
    if(total > maxMostrar*act_x_pag) $("#paginador").append("<div class='pag numPagsEtc2'>...</div>");
    else $("#paginador").append("<div class='pag numPagsEtc2 oculto'>...</div>");
    $("#paginador").append("<div class='pag numPags2' id='b_fin'>&#8594;</div>");
}

function getMensaje(num)
{
    if(num == 1)
    {
        if(idiomaG == 'en') return "No results";
        else if(idiomaG == 'fr') return "Aucun résultat";
        else return "No hay resultados";
    }
    else if(num == 2)
    {
        if(idiomaG == 'en') return "No subjects for this course";
        else if(idiomaG == 'fr') return "Aucun sujet de ce cours";
        else return "No existen asignaturas para este curso";
    }
    else if(num == 3)
    {
        if(idiomaG == 'en') return "We can not show the subjects of this course";
        else if(idiomaG == 'fr') return "Nous ne pouvons pas montrer les sujets de ce cours";
        else return "No podemos mostrarte las asignaturas de este curso";
    }
    else if(num == 4)
    {
        if(idiomaG == 'en') return "No activities that meet the established criteria";
        else if(idiomaG == 'fr') return "Aucune activité qui répondent aux critères établis";
        else return "No hay actividades que cumplan con los criterios establecidos";
    }
    else if(num == 5)
    {
        if(idiomaG == 'en') return "We can not show results for your criteria, please try again";
        else if(idiomaG == 'fr') return "Nous ne pouvons pas afficher de résultats pour vos critères, se il vous plaît essayer à nouveau";
        else return "No podemos mostrar resultados para tus criterios, vuelve a intentarlo";
    }
}

function cargaCantidadesCurso(cants)
{
    setTimeout(function(){
        
        $(".containerCursos .iListArrowLeft").hide();
        $(".containerCursos .headAccordion").hide();
        $(".containerCursos .cantCursos").text("");
        for(i=0;i<cants.length;i++)
        {
            $(".containerCursos .iListArrowLeft[data-id='"+cants[i]['ID']+"']").parents(".bodyAccordion").prev().show();
            $(".containerCursos .iListArrowLeft[data-id='"+cants[i]['ID']+"']").show();
            var elem = $(".containerCursos .cantCursos[data-id='"+cants[i]['ID']+"']");
            elem.text(cants[i]['TOTAL']);   
        }
        var cantNivel = 0;
        $(".containerCursos .headAccordion").each(function(){
            $(this).next().find(".cantCursos").each(function(){
               if($(this).text() != "") cantNivel += parseInt($(this).text()); 
            });
            $(this).find(".cantNivel").text('(' + cantNivel + ')');
            cantNivel = 0;
        });
    },100);
}

function cargaCantidadesEtag(cants)
{
    $('.moreResults').hide();    
    $(".containerAreaConocimiento .cantEtags").text("0");
    $(".containerAreaConocimiento .iListArrowLeft").attr("data-cantidad",0);
    $(".containerAreaConocimiento .iListArrowLeft").hide();
    for(i=0;i<cants.length;i++)
    {
        $(".containerAreaConocimiento .iListArrowLeft[data-id='"+cants[i]['ID']+"']").show();        
        $(".containerAreaConocimiento .iListArrowLeft[data-id='"+cants[i]['ID']+"']").attr('data-cantidad',cants[i]['TOTAL']);
        var elem = $(".containerAreaConocimiento .cantEtags[data-id='"+cants[i]['ID']+"']");
        elem.text(cants[i]['TOTAL']);   
    }
    tinysort('ul.navList>li',{selector:'a',attr:'data-cantidad',order:'desc'});
    
    var countItems = $('#resultados .navList li a[data-cantidad!=0]').length;
    var countItemsVisibles = 5;
    var cItems = 0;

    $('.moreResults').hide();
    $('#resultados .navList li').hide();
	if (countItems > countItemsVisibles) $('.moreResults').show();
    if (cItems < countItemsVisibles){
		for ( cItems; cItems <= countItemsVisibles; cItems++ ) {
			 $('#resultados .navList li:nth-child('+cItems+')').show();
		}
	}
    
    $('.moreResults a').unbind('click');
    $('.moreResults a').bind('click',function(e){
        e.preventDefault();
		countItemsVisibles = countItemsVisibles + 5;
		$('.moreResults').hide();
        if (countItems > countItemsVisibles) $('.moreResults').show();
		if (cItems < countItemsVisibles){
			for ( cItems; cItems <= countItemsVisibles; cItems++ ) {
				 $('#resultados .navList li:nth-child('+cItems+')').show();
			}
		}
	});
}

function cargaCantidadesAsig()
{
    var cants = cantidadesAsig;
    setTimeout(function(){
        
        $(".container-asignaturas .asignaturasEnlace").hide();
        $(".container-asignaturas .cantAsignaturas").text("");
        for(i=0;i<cants.length;i++)
        {
            $(".container-asignaturas input[data-id-asignatura='"+cants[i]['ID']+"']").parents("a").show();
            var elem = $(".container-asignaturas .cantAsignaturas[data-id='"+cants[i]['ID']+"']");
            elem.text(cants[i]['TOTAL']);   
        }
    },100);
}

function tieneEventoPopState()
{
    if(is_touch_device())
    {
        return true;
    }
    var ios = false;
    var p = navigator.platform;
    if( p === 'iPad' || p === 'iPhone' || p === 'iPod' ){
        ios = true;
    }
    return ios;
}

function is_touch_device() 
{
	var supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
	return supportsTouch;
}