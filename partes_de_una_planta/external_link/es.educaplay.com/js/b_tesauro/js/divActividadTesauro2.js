function generaActVis(arrayActividades)
{
    var cad = "";
    for(i=0;i<arrayActividades.length;i++)
    {
        var a = 'ri';
        if (i%2 == 0) a='le';
        
        var megusta = "Me gusta";
        var slug = 'recursoseducativos';
        var slug2 = 'coleccion';
        
        if(idiomaG == "fr") {
        	megusta = "J'aime";
        	slug = 'activiteeducatives';
        	slug2 = 'collection';
        } else if (idiomaG == "en") {
        	megusta = "I like";
        	slug = 'learningresources';
        	slug2 = 'collection';
        }
        
        var ruta;
        if (arrayActividades[i]['TIPO'] == 'COLECCION') {
        	ruta = idiomaG + "/" + slug2 + "/" + arrayActividades[i]['ID'] + "/1/" + arrayActividades[i]['TITULO_LIMPIO'] + ".htm";
        } else {
        	ruta = idiomaG + "/" + slug + "/" + arrayActividades[i]['ID'] + "/" + arrayActividades[i]['TITULO_LIMPIO'] + ".htm";
        }
        //http://fr.educaplay.devel/fr/collection/4287/1/actividades_1__eso.htm
        
        cad += "<div class='"+a+" boxbusact'>";
        cad += "<a class='le boxact "+getClase(arrayActividades[i]['TIPO'])+" fail' href='/"+idiomaG+"/recursoseducativos/"+arrayActividades[i]['ID']+"/"+arrayActividades[i]['TITULO_LIMPIO']+".htm' title='"+libGetFormatoTipo(arrayActividades[i]['TIPO'])+": "+arrayActividades[i]['TITULO']+"'> <strong>"+libGetFormatoTipo(arrayActividades[i]['TIPO'])+"</strong></a>";
        cad += "<div class='ri boxactder'>";
        cad += "<h3><a href='\\" + ruta + "' title='"+libGetFormatoTipo(arrayActividades[i]['TIPO'])+": "+arrayActividades[i]['TITULO']+"' class='fail'><strong>"+arrayActividades[i]['TITULO']+"</strong></a></h3>";
        cad += "<p class='iLike'>"+arrayActividades[i]['PUNTOS']+" "+megusta+"</p>";
        cad += "<p title='"+arrayActividades[i]['DESCRIPCION']+"'>"+arrayActividades[i]['DESCRIPCION_CORTADA']+"</p>";
        cad += "<a href='/"+idiomaG+"/mieducaplay/"+arrayActividades[i]['ID_AUTOR']+"/"+arrayActividades[i]['AUTOR_LIMPIO']+".htm' class='autor' title='Autor'>"+arrayActividades[i]['AUTOR']+"</a>";
        cad += "</div>";
        cad += "</div>";
        
        if(a=="ri") cad += "<div class='clear margen'></div>";
    }
    
    $("#listActividadesTes").html(cad); 
}

function getClase(tipo)
{
	if(tipo == "CRUCIGRAMA") return "actcruci";
    if(tipo == "SOPA") return "actsopa";
    if(tipo == "COMPLETAR") return "actcompletar";
    if(tipo == "ORDENAR_PALABRAS") return "actordpalabras";
    if(tipo == "ORDENAR_LETRAS") return "actordletras";
    if(tipo == "ADIVINANZA") return "actadivinanza";
    if(tipo == "DIALOGO") return "actdialogo";
    if(tipo == "DICTADO") return "actdictado";
    if(tipo == "RELACIONAR") return "actrelacionar";
    if(tipo == "TEST") return "acttest";
    if(tipo == "MAPA") return "actmapa";
    if(tipo == "PRESENTACION") return "actpresentacion";
    if(tipo == "VIDEOTEST") return "actvideotest";
    if(tipo == "RELACIONAR_COLUMNAS") return "actrelacionarcol";
    if(tipo == "RELACIONAR_MOSAICO") return "actrelacionarmos";
    if(tipo == "RULETA_PALABRAS") return "actruleta";
    if(tipo == "COLECCION") return "actcoleccion";    
}

function libGetFormatoTipo(tipo)
{
    if(idiomaG == "en")
    {
        if(tipo == "CRUCIGRAMA") return "Crossword";
        if(tipo == "SOPA") return "Wordsearch Puzzle";
        if(tipo == "COMPLETAR") return "Fill in Blanks";
        if(tipo == "ORDENAR_PALABRAS") return "Jumbled Sentence";
        if(tipo == "ORDENAR_LETRAS") return "Jumbled Word";
        if(tipo == "ADIVINANZA") return "Riddle";
        if(tipo == "DIALOGO") return "Dialogue";
        if(tipo == "DICTADO") return "Dictation";
        if(tipo == "RELACIONAR") return "Matching Game";
        if(tipo == "TEST") return "Quiz";
        if(tipo == "MAPA") return "Interactive Map";
        if(tipo == "PRESENTACION") return "Slide Show";
        if(tipo == "VIDEOTEST") return "Videoquiz";
        if(tipo == "RELACIONAR_COLUMNAS") return "Matching Columns Game";
        if(tipo == "RELACIONAR_MOSAICO") return "Matching Mosaic Game";
        if(tipo == "RULETA_PALABRAS") return "Alphabet Game";
        if(tipo == "COLECCION") return "Collection";
    }
    else if(idiomaG == "fr")
    {
        if(tipo == "CRUCIGRAMA") return "Mots Croisés";
        if(tipo == "SOPA") return "Mots Mêlés";
        if(tipo == "COMPLETAR") return "Compléter";
        if(tipo == "ORDENAR_PALABRAS") return "Ordonner les Mots";
        if(tipo == "ORDENAR_LETRAS") return "Ordonner les Lettres";
        if(tipo == "ADIVINANZA") return "Devinette";
        if(tipo == "DIALOGO") return "Dialogue";
        if(tipo == "DICTADO") return "Dictado";
        if(tipo == "RELACIONAR") return "Relier";
        if(tipo == "TEST") return "Test";
        if(tipo == "MAPA") return "Carte Interactive";
        if(tipo == "PRESENTACION") return "Présentacion";
        if(tipo == "VIDEOTEST") return "Videoquiz";
        if(tipo == "RELACIONAR_COLUMNAS") return "Relier Colonnes";
        if(tipo == "RELACIONAR_MOSAICO") return "Relier Mosaique";
        if(tipo == "RULETA_PALABRAS") return "Mots Roulette";
        if(tipo == "COLECCION") return "Collection";
    }
    else
    {
        if(tipo == "CRUCIGRAMA") return "Crucigrama";
        if(tipo == "SOPA") return "Sopa";
        if(tipo == "COMPLETAR") return "Completar";
        if(tipo == "ORDENAR_PALABRAS") return "Ordenar Palabras";
        if(tipo == "ORDENAR_LETRAS") return "Ordenar Letras";
        if(tipo == "ADIVINANZA") return "Adivinanza";
        if(tipo == "DIALOGO") return "Diálogo";
        if(tipo == "DICTADO") return "Dictado";
        if(tipo == "RELACIONAR") return "Relacionar";
        if(tipo == "TEST") return "Test";
        if(tipo == "MAPA") return "Mapa Interactivo";
        if(tipo == "PRESENTACION") return "Presentación";
        if(tipo == "VIDEOTEST") return "Videoquiz";
        if(tipo == "RELACIONAR_COLUMNAS") return "Relacionar Columnas";
        if(tipo == "RELACIONAR_MOSAICO") return "Relacionar Mosaico";
        if(tipo == "RULETA_PALABRAS") return "Ruleta de Palabras";
        if(tipo == "COLECCION") return "Colección";
    }     
}